defmodule Rushing.Repo.Migrations.CreatePlayers do
  use Ecto.Migration

  def change do
    create table(:players) do
      add :player_name, :string
      add :team, :string
      add :position, :string
      add :avg_rush_attempts_per_game, :float
      add :rush_attempts, :integer
      add :total_rush_yards, :integer
      add :avg_rush_yards_per_attempt, :float
      add :rush_yards_per_game, :float
      add :total_rush_touchdowns, :integer
      add :longest_rush, :integer
      add :rushing_first_downs, :integer
      add :rushing_first_down_percent, :float
      add :rushing_over_20, :integer
      add :rushing_over_40, :integer
      add :fumbles, :integer

      timestamps()
    end
  end
end
