if Rushing.Players.all() == [] do
  "rushing.json"
  |> File.read!()
  |> Jason.decode!()
  |> Enum.map(fn json_player ->
    %{
      "Player" => player_name,
      "Team" => team,
      "Pos" => position,
      "Att" => rush_attempts,
      "Att/G" => avg_rush_attempts_per_game,
      "Yds" => total_rush_yards,
      "Avg" => avg_rush_yards_per_attempt,
      "Yds/G" => rush_yards_per_game,
      "TD" => total_rush_touchdowns,
      "Lng" => longest_rush,
      "1st" => rushing_first_downs,
      "1st%" => rushing_first_down_percent,
      "20+" => rushing_over_20,
      "40+" => rushing_over_40,
      "FUM" => fumbles
    } = json_player

    %{
      player_name: player_name,
      team: team,
      position: position,
      avg_rush_attempts_per_game: avg_rush_attempts_per_game,
      rush_attempts: rush_attempts,
      total_rush_yards: total_rush_yards,
      avg_rush_yards_per_attempt: avg_rush_yards_per_attempt,
      rush_yards_per_game: rush_yards_per_game,
      total_rush_touchdowns: total_rush_touchdowns,
      longest_rush: longest_rush,
      rushing_first_downs: rushing_first_downs,
      rushing_first_down_percent: rushing_first_down_percent,
      rushing_over_20: rushing_over_20,
      rushing_over_40: rushing_over_40,
      fumbles: fumbles
    }
    |> Rushing.Helpers.validate()
    |> Rushing.Players.new()
    |> Rushing.Repo.insert!()
  end)
end
