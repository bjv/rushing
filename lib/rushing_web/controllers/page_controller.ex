defmodule RushingWeb.PageController do
  use RushingWeb, :controller

  alias NimbleCSV.RFC4180, as: CSV
  alias Rushing.Helpers
  alias Rushing.Players

  @keys [
    :player_name,
    :team,
    :position,
    :avg_rush_attempts_per_game,
    :rush_attempts,
    :total_rush_yards,
    :avg_rush_yards_per_attempt,
    :rush_yards_per_game,
    :total_rush_touchdowns,
    :longest_rush,
    :rushing_first_downs,
    :rushing_first_down_percent,
    :rushing_over_20,
    :rushing_over_40,
    :fumbles
  ]

  @header [
    "Att/G",
    "Avg",
    "FUM",
    "Lng",
    "Player",
    "Pos",
    "Att",
    "Yds/G",
    "1st%",
    "1st",
    "20+",
    "40+",
    "Team",
    "TD",
    "Yds"
  ]

  def download(conn, params) do
    sort_by = Map.get(params, :sort_by, nil)
    filter = Map.get(params, :filter, nil)

    sorted_players = get_players(filter, sort_by)

    csv = to_csv(sorted_players)

    conn
    |> put_resp_content_type("text/csv")
    |> put_resp_header("content-disposition", "attachment; filename=\"rushing.csv\"")
    |> send_resp(200, csv)
  end

  defp get_players(filter, sort_by) do
    cond do
      filter ->
        Players.get_by_name(filter)

      sort_by ->
        Players.all()
        |> Helpers.sort_players(sort_by)

      true ->
        Players.all()
    end
    |> Enum.map(fn player -> Map.take(player, @keys) end)
  end

  defp to_csv(players) do
    csv_data =
      players
      |> Enum.map(fn player ->
        Enum.map(player, &get_player_values_as_string/1)
      end)

    [@header | csv_data]
    |> CSV.dump_to_iodata()
  end

  defp get_player_values_as_string({_key, value}) when is_float(value), do: Float.to_string(value)

  defp get_player_values_as_string({_key, value}) when is_integer(value),
    do: Integer.to_string(value)

  defp get_player_values_as_string({_key, value}), do: value
end
