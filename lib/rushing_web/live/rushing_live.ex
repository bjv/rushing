defmodule RushingWeb.RushingLive do
  use Phoenix.LiveView

  alias Rushing.Helpers
  alias Rushing.Players

  def render(assigns) do
    Phoenix.View.render(RushingWeb.PageView, "index.html", assigns)
  end

  def mount(_params, _session, socket) do
    assigns = [
      players: Rushing.Players.all(),
      errors: [],
      sort_by: "",
      filter: ""
    ]

    {:ok, assign(socket, assigns)}
  end

  def handle_params(%{"sort_by" => sort_by}, _uri, socket) do
    case sort_by do
      sort_by when sort_by in ~w(total_rush_yards longest_rush total_rush_touchdowns) ->
        {:noreply,
         assign(socket,
           players: Helpers.sort_players(socket.assigns.players, sort_by),
           sort_by: sort_by
         )}

      _ ->
        {:noreply, socket}
    end
  end

  # Would be good to handle partial matches but have to be careful of SQL injection
  def handle_params(%{"search_field" => %{"query" => query}}, _, socket) when query == "" do
    {:noreply, socket}
  end

  def handle_params(%{"search_field" => %{"query" => query}}, _, socket) do
    case Players.get_by_name(query) do
      nil ->
        {:noreply,
         assign(socket, :errors, [
           "Player not found, try searching for a player by their full name"
         ])}

      player ->
        assigns = [
          players: [player],
          filter: query
        ]

        {:noreply, assign(socket, assigns)}
    end
  end

  def handle_params(_params, _uri, socket) do
    {:noreply, socket}
  end
end
