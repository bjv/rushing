defmodule Rushing.Helpers do
  @doc """
  Validates value types are what the database expects,
  casting appropriately if they are not
  """
  def validate(player) do
    player
    |> validate_rush_yds_per_game()
    |> validate_total_rush_yds()
    |> validate_longest_rush()
  end

  defp validate_rush_yds_per_game(%{rush_yards_per_game: rush_yds_per_game} = player) do
    case rush_yds_per_game do
      yards when is_integer(yards) ->
        # convert int to float
        Map.put(player, :rush_yards_per_game, yards / 1)

      yards when is_float(yards) ->
        player

      yards when is_binary(yards) ->
        {float_yards, _} = Float.parse(yards)
        Map.put(player, :rush_yards_per_game, float_yards)
    end
  end

  defp validate_total_rush_yds(%{total_rush_yards: total_rush_yds} = player) do
    case total_rush_yds do
      yards when is_integer(yards) ->
        player

      yards when is_binary(yards) ->
        {int_yards, _} =
          yards
          |> String.replace(",", "")
          |> Integer.parse()

        Map.put(player, :total_rush_yards, int_yards)
    end
  end

  defp validate_longest_rush(%{longest_rush: longest_rush} = player) do
    case longest_rush do
      yards when is_integer(yards) ->
        player

      yards when is_binary(yards) ->
        # Add logic here when we care about longest rushing touchdowns
        {longest, _touchdown} = Integer.parse(yards)

        Map.put(player, :longest_rush, longest)
    end
  end

  @doc """
  Sorts an Enumerable of players based on the passed in field.
  Hard coded to descending order, but could be extended in the future if required
  """
  @spec sort_players(Enum.t(), String.t()) :: Enum.t()
  def sort_players(players, sort_key)

  def sort_players(players, "total_rush_yards") do
    Enum.sort_by(players, fn player -> player.total_rush_yards end, :desc)
  end

  def sort_players(players, "longest_rush") do
    Enum.sort_by(players, fn player -> player.longest_rush end, :desc)
  end

  def sort_players(players, "total_rush_touchdowns") do
    Enum.sort_by(
      players,
      fn player -> player.total_rush_touchdowns end,
      :desc
    )
  end
end
