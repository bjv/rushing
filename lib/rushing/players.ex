defmodule Rushing.Players do
  alias Rushing.Players.Player
  alias Rushing.Repo

  def new(attrs) do
    Player.changeset(%Player{}, attrs)
  end

  def all do
    Repo.all(Player)
  end

  def get_by_name(name) do
    Repo.get_by(Player, player_name: name)
  end
end
