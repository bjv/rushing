defmodule Rushing.Players.Player do
  use Ecto.Schema
  import Ecto.Changeset

  schema "players" do
    field :avg_rush_attempts_per_game, :float
    field :avg_rush_yards_per_attempt, :float
    field :fumbles, :integer
    field :longest_rush, :integer
    field :player_name, :string
    field :position, :string
    field :rush_attempts, :integer
    field :rush_yards_per_game, :float
    field :rushing_first_down_percent, :float
    field :rushing_first_downs, :integer
    field :rushing_over_20, :integer
    field :rushing_over_40, :integer
    field :team, :string
    field :total_rush_touchdowns, :integer
    field :total_rush_yards, :integer

    timestamps()
  end

  @doc false
  def changeset(player, attrs) do
    player
    |> cast(attrs, [
      :player_name,
      :team,
      :position,
      :avg_rush_attempts_per_game,
      :rush_attempts,
      :total_rush_yards,
      :avg_rush_yards_per_attempt,
      :rush_yards_per_game,
      :total_rush_touchdowns,
      :longest_rush,
      :rushing_first_downs,
      :rushing_first_down_percent,
      :rushing_over_20,
      :rushing_over_40,
      :fumbles
    ])
    |> validate_required([
      :player_name,
      :team,
      :position,
      :avg_rush_attempts_per_game,
      :rush_attempts,
      :total_rush_yards,
      :avg_rush_yards_per_attempt,
      :rush_yards_per_game,
      :total_rush_touchdowns,
      :longest_rush,
      :rushing_first_downs,
      :rushing_first_down_percent,
      :rushing_over_20,
      :rushing_over_40,
      :fumbles
    ])
  end
end
